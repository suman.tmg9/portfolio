import { useState } from 'react';
import './works.scss';

export default function Works() {
  const [currentSlide, setCurrentSlide] = useState(0);
  const data = [
    {
      id: '1',
      icon: './assets/mobile.png',
      title: 'Web Design',
      linkTo: 'https://arkhabar.com/',
      desc: 'We will do remarkable responsive web development in react js, nodejs, html5, css3, json ,Django and in wordpress',
      img: './assets/website/shopify/Capture.PNG',
    },
    {
      id: '2',
      icon: './assets/globe.png',
      title: 'Mobile Application',
      linkTo:
        'https://drive.google.com/file/d/13ILNcKrtNB_Pt17VZIeR1ICF8mI60klc/view?fbclid=IwAR0KK4Lzf6Y_YtRv7ug_-1CczsSg_fCjPVNH97Y9WZ9otafcv3tTTB0mROc',
      desc: 'Your idea of a smartphone app that runs on iOS and Android will be materialised professionally using React Native which is a leading industry-standard technology developed by Facebook.',
      img: './assets/app/markitup/186544475_171325104920435_4815428466103367854_n.jpg',
    },
    {
      id: '3',
      icon: './assets/writing.png',
      title: 'Branding',
      linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
      desc: 'We will develop the marketing and branding strategy for your brand',
      img: 'https://fiverr-res.cloudinary.com/images/t_smartwm/t_main1,q_auto,f_auto,q_auto,f_auto/attachments/delivery/asset/e7c453999658fd9b6aaf19d04b9508b8-1579505992/banner/can-design-logo-and-banner.jpg',
    },
  ];

  const handleClick = (way) => {
    way === 'left'
      ? setCurrentSlide(currentSlide > 0 ? currentSlide - 1 : 2)
      : setCurrentSlide(currentSlide < data.length - 1 ? currentSlide + 1 : 0);
  };

  return (
    <div className='works' id='works'>
      <div
        className='slider'
        style={{ transform: `translateX(-${currentSlide * 100}vw)` }}
      >
        {data.map((d) => (
          <div className='container'>
            <div className='item'>
              <div className='left'>
                <div className='leftContainer'>
                  <div className='imgContainer'>
                    <img src={d.icon} alt='' />
                  </div>
                  <h2>{d.title}</h2>
                  <p>{d.desc}</p>
                  <span>
                    <a href={d.linkTo} target='_blank'>
                      Visit
                    </a>
                  </span>
                </div>
              </div>
              <div className='right'>
                <img src={d.img} alt='' />
              </div>
            </div>
          </div>
        ))}
      </div>
      <img
        src='assets/arrow.png'
        className='arrow left'
        alt=''
        onClick={() => handleClick('left')}
      />
      <img
        src='assets/arrow.png'
        className='arrow right'
        alt=''
        onClick={() => handleClick()}
      />
    </div>
  );
}
