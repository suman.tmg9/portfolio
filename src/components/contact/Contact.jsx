import { useState, useEffect } from 'react';
import Button from '@material-ui/core/Button';
import './contact.scss';

export default function Contact() {
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [msg, setMsg] = useState('');
  const [sending, setSending] = useState(false);

  useEffect(() => {
    setTimeout(() => {
      setMsg('');
    }, 5000);
  }, [msg]);

  const resetForm = () => {
    setEmail('');
    setMessage('');
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    const regx = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/g;
    if (!regx.test(email)) {
      setMsg('Invalid Email!!!');
    } else if (!message) setMsg('Message is empty!!!');
    else {
      setSending(true);
      setMsg('Sending...');
      try {
        const res = await fetch(
          'https://ashalaa-portfolio.herokuapp.com/send-email',
          {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({ email, message }),
          }
        );
        const resp = await res.json();
        setSending(false);
        setMsg(resp.message);
        resetForm();
      } catch (e) {
        setSending(false);
        setMsg('Something went wrong :( Please try again');
      }
    }
  };
  return (
    <div className='contact' id='contact'>
      <div className='left'>
        <img src='assets/shake.svg' alt='' />
      </div>
      <div className='right'>
        <h2>Contact.</h2>
        <form onSubmit={handleSubmit}>
          <input
            type='text'
            placeholder='Email'
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
          <textarea
            placeholder='Message'
            value={message}
            onChange={(e) => setMessage(e.target.value)}
          ></textarea>
          <Button
            type='submit'
            style={{ width: '25%' }}
            variant='outlined'
            color='secondary'
            disabled={sending}
          >
            Send
          </Button>
          {msg && <span style={{ color: '#F4BE2C', fontSize: 20 }}>{msg}</span>}
        </form>
      </div>
    </div>
  );
}
