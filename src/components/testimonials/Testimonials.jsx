import './testimonials.scss';
import Button from '@material-ui/core/Button';

export default function Testimonials() {
  const data = [
    {
      id: 1,
      name: 'Tom Durden',
      title: 'Senior Developer',
      type: 'Basic',
      services: ['Website Audit', 'Design', 'Development'],
    },
    {
      id: 2,
      name: 'Alex Kalinski',
      title: 'Co-Founder of DELKA',
      type: 'Standard',
      services: [
        'Website Audit',
        'Design',
        'Development',
        'Backups',
        'Analytics',
        'Live Chat',
        'Search Engine Optimization',
        'Content Management',
      ],
      featured: true,
    },
    {
      id: 3,
      name: 'Martin Harold',
      title: 'CEO of ALBI',
      type: 'Premium',
      services: [
        'Website Audit',
        'Design',
        'Development',
        'Backups',
        'Analytics',
        'Live Chat',
        'Search Engine Optimization',
        'Content Management',
        'eCommerce',
        'Hosting',
      ],
    },
  ];
  return (
    <div className='testimonials' id='testimonials'>
      <h1>Pricing</h1>
      <div className='container'>
        {data.map((d) => (
          <div className={d.featured ? 'card featured' : 'card'}>
            <div className='top'>
              {/* <img src="assets/right-arrow.png" className="left" alt="" />
              <img
                className="user"
                src={d.img}
                alt=""
              />
              <img className="right" src={d.icon} alt="" /> */}
              <h1>{d.type}</h1>
            </div>
            <div className='center'>
              {d.services.map((s, i) => (
                <p key={i}>{s}</p>
              ))}
            </div>
            <div className='bottom'>
              {/* <h3>{d.name}</h3>
              <h4>{d.title}</h4> */}
              <a href='#contact'>
                <Button variant='contained' color='secondary'>
                  Request Quote
                </Button>
              </a>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
