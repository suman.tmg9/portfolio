import "./topbar.scss";
import { Person, Mail } from "@material-ui/icons";
import FacebookIcon from '@material-ui/icons/Facebook';
import YouTubeIcon from '@material-ui/icons/YouTube';


export default function Topbar({ menuOpen, setMenuOpen }) {
  return (
    <div className={"topbar " + (menuOpen && "active")}>
      <div className="wrapper">
        <div className="left">
          <a href="#intro" className="logo">
            Ashalaa.
          </a>
          <div className="itemContainer">
            <Person className="icon" />
            <span>+9779813036294</span>
          </div>
          <div className="itemContainer">
            <Mail className="icon" />
            <span>info.ashalaa@gmail.com</span>
          </div>
          <a style={{textDecoration:'none' , color:'#091919'}} href="https://www.facebook.com/AshalaaDevelopers" target="_blank">
            <div className="itemContainer">
            
              <FacebookIcon className="icon"  />
            <span>Ashalaa Technology</span>
          </div></a>
          <a style={{textDecoration:'none' , color:'#091919'}} href="https://www.youtube.com/channel/UCl4fjWqCxXqDIiwZxPL0chQ" target="_blank">
            <div className="itemContainer">
            
              <YouTubeIcon className="icon"  />
            <span>Ashalaa Technology</span>
          </div></a>
        </div>
        <div className="right">
          <div className="hamburger" onClick={()=>setMenuOpen(!menuOpen)}>
            <span className="line1"></span>
            <span className="line2"></span>
            <span className="line3"></span>
          </div>
        </div>
      </div>
    </div>
  );
}
