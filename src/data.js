export const featuredPortfolio = [
  {
    id: 1,
    title: 'Blood bank App',
    linkTo:
      'https://drive.google.com/drive/folders/14a9iaeJwAM0vvvmVX-Tg0NKfsxKl305v?usp=sharing',
    img: '/assets/app/bloodbank/4f32eb1c-3080-4b6a-8abc-6fa1369edbe9.jpg ',
  },
  {
    id: 2,
    title: 'College Banner',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: 'https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/122919322/original/67fc58cdf1cdc3c2e221c5988a01373cbc5cda5a/can-design-logo-and-banner.jpg',
  },
  {
    id: 3,
    title: 'E-commerce Web Design',
    linkTo:
      'https://drive.google.com/drive/folders/1TWWbog20D2mChXuhPWmlCzGUuswlaC1t?usp=sharing',
    img: '/assets/website/ecommerce/7c3596ed-a410-41bf-9e49-24550e5c4d13.jpg',
  },
  {
    id: 4,
    title: 'Wordpress web design',
    linkTo: 'https://gloabaldem.com/',
    img: '/assets/website/globaldem/Capture.PNG',
  },
  {
    id: 5,
    title: 'Pizza Order Web App',
    linkTo:
      'https://drive.google.com/drive/folders/1BrytGoiG81r_aLtlFX1n53HrDw2Rpt75?usp=sharing',
    img: '/assets/website/pizzaorder/0a7c7d69-a2e1-4ec2-bb27-d7cd62392396.jpg',
  },
  {
    id: 6,
    title: 'Markit Up Mobile app',
    linkTo:
      'https://drive.google.com/file/d/13ILNcKrtNB_Pt17VZIeR1ICF8mI60klc/view?fbclid=IwAR0KK4Lzf6Y_YtRv7ug_-1CczsSg_fCjPVNH97Y9WZ9otafcv3tTTB0mROc',
    img: '/assets/app/markitup/186544475_171325104920435_4815428466103367854_n.jpg',
  },
];

export const webPortfolio = [
  {
    id: 1,
    title: 'Institute Website',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: '/assets/website/advlearning/3d0b26f8-c603-4980-8626-9e0132639a0f.jpg',
  },
  {
    id: 2,
    title: 'Ecommerce Website',
    linkTo:
      'https://drive.google.com/drive/folders/19nJ-FY0khyq07hFlvcHJwJNClDDPo6Db?usp=sharing',
    img: '/assets/website/ecommerce/7c3596ed-a410-41bf-9e49-24550e5c4d13.jpg',
  },
  {
    id: 3,
    title: 'Wordpress website Design',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: '/assets/website/globaldem/Capture.PNG',
  },
  {
    id: 4,
    title: 'Pizza ordering web app',
    linkTo:
      'https://drive.google.com/drive/folders/1BrytGoiG81r_aLtlFX1n53HrDw2Rpt75?usp=sharing',
    img: '/assets/website/pizzaorder/0a7c7d69-a2e1-4ec2-bb27-d7cd62392396.jpg',
  },
  {
    id: 5,
    title: 'Personal Webiste',
    linkTo:
      'https://drive.google.com/drive/folders/1Ym0pSNZMXDIqhzNp3YPZ6GL1kJ8AWrGI?usp=sharing',
    img: '/assets/website/ninjanote/55529f66-68f7-40c9-bd2c-e4aaec0354b7.jpg',
  },
  {
    id: 6,
    title: 'News portal Website',
    linkTo: 'https://arkhabar.com/',
    img: '/assets/website/shopify/Capture.PNG',
  },
];

export const mobilePortfolio = [
  {
    id: 1,
    title: 'Blood Bank app',
    linkTo:
      'https://drive.google.com/drive/folders/14a9iaeJwAM0vvvmVX-Tg0NKfsxKl305v?usp=sharing',
    img: '/assets/app/bloodbank/4f32eb1c-3080-4b6a-8abc-6fa1369edbe9.jpg',
  },
  {
    id: 2,
    title: 'Chat App',
    linkTo:
      'https://drive.google.com/drive/folders/19IzIRy6Hif8dY1OVJqB2dZUvgnpYqWLd?usp=sharing',
    img: 'assets/app/chatapp/0047eaf9-41b3-45d7-912b-ab319eb2bbcf.jpg',
  },
  {
    id: 3,
    title: 'Covid tracker app',
    linkTo:
      'https://drive.google.com/file/d/1pVCO6JC2gVwoQ_ahxrngWBHz876qNWXm/view?fbclid=IwAR17r9QSreO7K-lf5OxfDsrUXfpSDMkwvuWZg4fVq-vZKPwxSp0V8B8bKds',
    img: '/assets/app/covidtracker/187997559_1248509695552144_1179655102182495024_n.jpg',
  },
  {
    id: 4,
    title: 'MarkIt App',
    linkTo:
      'https://drive.google.com/file/d/13ILNcKrtNB_Pt17VZIeR1ICF8mI60klc/view?fbclid=IwAR0KK4Lzf6Y_YtRv7ug_-1CczsSg_fCjPVNH97Y9WZ9otafcv3tTTB0mROc',
    img: '/assets/app/markitup/186544475_171325104920435_4815428466103367854_n.jpg',
  },
  {
    id: 5,
    title: 'Wallpaper app',
    linkTo:
      'https://drive.google.com/drive/folders/189KQKrDwWDsTtff2St569AbqqGgJpJ96?usp=sharing',
    img: '/assets/app/wallpaperapp/f73060b4-8267-4b64-bada-3514b0d4e753.jpg',
  },
  {
    id: 6,
    title: 'whatsapp clone',
    linkTo:
      'https://drive.google.com/drive/folders/1iQ44NXkZAQeXsvC0QUfbv7rvmZtJeqib?usp=sharing',
    img: '/assets/app/whatsappclone/524f7734-a8b5-4bc1-b524-16f742b6b133.jpg',
  },
];

export const designPortfolio = [
  {
    id: 1,
    title: 'Design College Banner',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: 'https://fiverr-res.cloudinary.com/images/q_auto,f_auto/gigs/122919322/original/67fc58cdf1cdc3c2e221c5988a01373cbc5cda5a/can-design-logo-and-banner.jpg',
  },
  {
    id: 2,
    title: 'Design Company Logo',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: 'https://fiverr-res.cloudinary.com/images/t_smartwm/t_main1,q_auto,f_auto,q_auto,f_auto/attachments/delivery/asset/7c6dc5f11753a1233e76aaea59e0b913-1606534058/mockup2jpg%20-%20Copy/can-design-logo-and-banner.jpg',
  },
  {
    id: 3,
    title: 'Design Football Club Logo',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: 'https://fiverr-res.cloudinary.com/images/t_smartwm/t_main1,q_auto,f_auto,q_auto,f_auto/attachments/delivery/asset/23693a931495bfc4816f0c7e9e3b980e-1583326977/banner/can-design-logo-and-banner.png',
  },
  {
    id: 4,
    title: 'Design Social Media Banner',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: 'https://fiverr-res.cloudinary.com/images/t_smartwm/t_main1,q_auto,f_auto,q_auto,f_auto/attachments/delivery/asset/e7c453999658fd9b6aaf19d04b9508b8-1579505992/banner/can-design-logo-and-banner.jpg',
  },
  {
    id: 5,
    title: 'Logo Desing',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: 'https://fiverr-res.cloudinary.com/images/t_smartwm/t_main1,q_auto,f_auto,q_auto,f_auto/attachments/delivery/asset/708b3f322059e8417c2af6a3f16a665c-1610598327/12/can-design-logo-and-banner.png',
  },
  {
    id: 6,
    title: 'Logo Design',
    linkTo: 'https://www.fiverr.com/sumantamang/can-design-logo-and-banner',
    img: 'https://fiverr-res.cloudinary.com/images/t_smartwm/t_main1,q_auto,f_auto,q_auto,f_auto/attachments/delivery/asset/745bde749095e45868f50a88a852219f-1609653621/profilepicture/can-design-logo-and-banner.jpg',
  },
];

export const contentPortfolio = [
  {
    id: 1,
    title: 'Content Social Media App',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU',
  },
  {
    id: 2,
    title: 'Content Rampa UI Design',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU',
  },
  {
    id: 3,
    title: 'Content E-commerce Web Design',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU',
  },
  {
    id: 4,
    title: 'Content Relax Mobile App',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU',
  },
  {
    id: 5,
    title: 'Content Keser Web Design',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU',
  },
  {
    id: 6,
    title: 'Content Banking App',
    img: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKvdHn8GmPGCO0y3SJqNHACygpm0h9VycMHg&usqp=CAU',
  },
];
